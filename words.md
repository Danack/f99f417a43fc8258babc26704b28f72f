Apropos of everything; I have strong feelings about how sponsorship levels are described.

Too many maintainers are asking for people to 'buy a coffee' or 'show you're a fan'.

This is folksy nonsense that suggests that individuals are those who should be sponsoring open source.

Companies are the entities that profit from open source, they are the ones who should be funding it. Phrasing it more 'professionally' avoids putting pressure on individuals, and can lead to larger sponsorship arriving.

The sponsor levels I have are:

$1 a month - This tier is appropriate if you are a solo developer.

$5 a month - This tier is appropriate if you are a solo developer, but can spend more than $1 a month.

$10 a month - This tier is appropriate if your company is tiny (<5 employees).

$50 a month - This tier is appropriate if your company is small (<10 employees).

$100 a month - This tier is appropriate if your company is medium (<50 employees). Priority will be given to your bug reports and issues.

$250 a month - At this tier, priority will be given to your bug reports and issues.

$500 a month - At this tier, high priority will be given to your bug reports and issues.

$1,000 a month - This tier is appropriate if your company is large or you are a hosting company. At this tier, priority will be given to your bug reports and issues. You will also be able to suggest what work items should be worked on.

$2,000 a month - At this tier, priority will be given to your bug reports and issues. You will also be able to suggest what work items should be worked on. If you wish, you can also have a monthly call to discuss the roadmap of work items to be focused on.

$6,000 a month - At this tier, priority will be given to your bug reports and issues. You will also be able to suggest what work items should be worked on. If you wish, you can also have a monthly call to discuss the roadmap of work items to be focused on, and priority will be given to items you think should be worked on.

I realise that I'm in a lucky position as the main lib I look after is unforkable (due to PHP pecl being one of the worst distribution methods), but having >$1000 sponsor tiers should be done by all projects.